package com.ibsu.demo.repositories;

import com.ibsu.demo.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
    //Employee findOneByEmployeeId(Long employeeId);
    //List<Employee> findAll();
}
